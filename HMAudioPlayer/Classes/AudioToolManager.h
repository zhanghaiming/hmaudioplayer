//
//  AudioToolManager.h
//  FBSnapshotTestCase
//
//  Created by zhm on 2018/12/19.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AudioToolManager : NSObject

@property(nonatomic,strong) NSString *audioName;

- (NSArray *)fetchAudioList;

@end


