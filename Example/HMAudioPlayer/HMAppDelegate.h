//
//  HMAppDelegate.h
//  HMAudioPlayer
//
//  Created by 张海明 on 12/19/2018.
//  Copyright (c) 2018 张海明. All rights reserved.
//

@import UIKit;

@interface HMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
