//
//  HMViewController.m
//  HMAudioPlayer
//
//  Created by 张海明 on 12/19/2018.
//  Copyright (c) 2018 张海明. All rights reserved.
//

#import "HMViewController.h"
#import "AudioToolManager.h"
#import "AudioViewController.h"

@interface HMViewController ()


@end

@implementation HMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
}

- (IBAction)btnPress:(id)sender {
    
    AudioToolManager *toolManager = [[AudioToolManager alloc] init];
    
    NSArray *audioList = [toolManager fetchAudioList];
//    NSLog(@"audioList:%@",audioList);
    AudioViewController *audioController = [[AudioViewController alloc] init];
    audioController.view.backgroundColor = [UIColor blueColor];
    [self.navigationController pushViewController:audioController animated:YES];
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
