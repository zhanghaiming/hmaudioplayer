#
# Be sure to run `pod lib lint HMAudioPlayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HMAudioPlayer'
  s.version          = '0.1.5'
  s.summary          = '一个通用的音频播放器'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
这是一款通用的音频播放器，一个通用的音频播放器
                       DESC

  s.homepage         = 'https://gitlab.com/zhanghaiming/hmaudioplayer'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '张海明' => '1938708066@qq.com' }
  s.source           = { :git => 'https://gitlab.com/zhanghaiming/hmaudioplayer.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'HMAudioPlayer/Classes/**/*'
  
   s.resource_bundles = {
     'HMAudioPlayer' => ['HMAudioPlayer/Assets/*.png','HMAudioPlayer/xibs/*.xib']
   }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
